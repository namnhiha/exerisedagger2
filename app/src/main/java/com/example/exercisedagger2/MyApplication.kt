package com.example.exercisedagger2

import android.app.Application
import com.example.exercisedagger2.di.DaggerApplicationComponent

class MyApplication:Application(){
    val appComponent = DaggerApplicationComponent.create()
}
