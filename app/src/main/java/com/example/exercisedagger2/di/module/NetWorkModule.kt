package com.example.exercisedagger2.di.module

import com.example.exercisedagger2.data.repository.remote.LoginRetrofitService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class NetWorkModule {

    @Singleton
    @Provides
    fun provideLoginRetrofitService(): LoginRetrofitService {
        return Retrofit.Builder()
            .baseUrl("https://example.com")
            .build()
            .create(LoginRetrofitService::class.java)
    }
}
