package com.example.exercisedagger2.di

import com.example.exercisedagger2.di.module.NetWorkModule
import com.example.exercisedagger2.ui.LoginActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetWorkModule::class])
interface ApplicationComponent {
    fun inject(activity:LoginActivity)
}
