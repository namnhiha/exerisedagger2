package com.example.exercisedagger2.data

import com.example.exercisedagger2.data.repository.local.UserLocalDataSource
import com.example.exercisedagger2.data.repository.remote.UserRemoteDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
    private var localDataSource: UserLocalDataSource,
    private var remoteDataSource: UserRemoteDataSource
) {}
