package com.example.exercisedagger2.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.exercisedagger2.MyApplication
import com.example.exercisedagger2.R
import javax.inject.Inject

class LoginActivity : AppCompatActivity() {
    @Inject
    lateinit var loginViewModel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (applicationContext as MyApplication).appComponent.inject(this)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
    }
}
